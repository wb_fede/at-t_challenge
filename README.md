# At&tChallenge

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3.
Assessment description available in ATT Cybersecurity - Ejercicio Frontend dev.pdf

## Install dependencies
Run `npm install` to install dependencies

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test --browsers ChromeHeadless` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Run
Run server with `ng serve` and the result will be available in the console:
```
FullUser {id: '123', name: 'Homer Simpsons', address: 'Evergreen Terrace', number: 742}
FullUser {id: '456', name: 'Ned Flanders', address: 'Evergreen Terrace', number: 744}
```