import { getTestBed, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { UserService } from './user.service';

describe('UserService', () => {
  let injector: TestBed;
  let service: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[
        HttpClientTestingModule
      ]
    });
    injector = getTestBed();
    service = TestBed.inject(UserService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getUserName function', () => {
    it('Should call service and return user', () => {
      const dummyUser = {
          "id": "123",
          "name": "Homer Simpsons",
          "addressId": "456"
      }
  
      service.getUserName("123").subscribe(user => {
        expect(user.id).toBe("123");
        expect(user.name).toEqual("Homer Simpsons");
        expect(user.addressId).toEqual("456");
      });
  
      const req = httpMock.expectOne(`/assets/mocks/123-name.json`);
      expect(req.request.method).toBe("GET");
      req.flush(dummyUser);
    })
  });

  describe('getUserAddress function', () => {
    it('Should call service and return user', () => {
      const dummyUser = {
        "address": "Evergreen Terrace",
        "number": 742
      }
  
      service.getUserAddress("123").subscribe(user => {
        expect(user.address).toBe("Evergreen Terrace");
        expect(user.number).toEqual(742);
      });
  
      const req = httpMock.expectOne(`/assets/mocks/123-address.json`);
      expect(req.request.method).toBe("GET");
      req.flush(dummyUser);
    })
  });

  describe('getUser function', () => {
    it('Should call service and return user', () => {
      const dummyUser = {
        "id": "123",
        "name": "Homer Simpsons",
        "addressId": "456"
    }
      const dummyAddress = {
        "address": "Evergreen Terrace",
        "number": 742
      }
  
      service.getUser("123").subscribe(user => {
        expect(user.id).toBe("123");
        expect(user.name).toEqual("Homer Simpsons");
        expect(user.address).toBe("Evergreen Terrace");
        expect(user.number).toEqual(742);
      });
  
      const reqUser = httpMock.expectOne(`/assets/mocks/123-name.json`);
      expect(reqUser.request.method).toBe("GET");
      reqUser.flush(dummyUser);

      const reqAddress = httpMock.expectOne(`/assets/mocks/123-address.json`);
      expect(reqAddress.request.method).toBe("GET");
      reqAddress.flush(dummyAddress);
    })
  });

  describe('getUserWithAddress function', () => {
    it('Should call service and return user with the addressId', () => {
      const dummyUser = {
        "id": "123",
        "name": "Homer Simpsons",
        "addressId": "456"
    }
      const dummyAddress = {
        "address": "Evergreen Terrace",
        "number": 742
      }
  
      service.getUserWithAddress("123").subscribe(user => {
        expect(user.id).toBe("123");
        expect(user.name).toEqual("Homer Simpsons");
        expect(user.address).toBe("Evergreen Terrace");
        expect(user.number).toEqual(742);
      });
  
      const reqUser = httpMock.expectOne(`/assets/mocks/123-name.json`);
      expect(reqUser.request.method).toBe("GET");
      reqUser.flush(dummyUser);

      const reqAddress = httpMock.expectOne(`/assets/mocks/456-address.json`);
      expect(reqAddress.request.method).toBe("GET");
      reqAddress.flush(dummyAddress);
    })

    it('Catch error if address id does not exist', () => {
      const dummyUser = {
        "id": "123",
        "name": "Homer Simpsons"
    }
      const dummyAddress = {
        "address": "Evergreen Terrace",
        "number": 742
      }
  
      service.getUserWithAddress("123").subscribe(user => {
      }, err => {
        expect(err.message).toBe('NO_ADDRESS_ID')
      });
  
      const reqUser = httpMock.expectOne(`/assets/mocks/123-name.json`);
      expect(reqUser.request.method).toBe("GET");
      reqUser.flush(dummyUser);
    })
  });
});
