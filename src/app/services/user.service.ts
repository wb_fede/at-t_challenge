import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { FullUser, IUser, IUserAddress } from '../entities/user.class';
import { forkJoin, map, mergeMap, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Get user name and id
   * @param {string} id user id
   * @returns user object
   */
  getUserName(id: string): Observable<IUser>{
    return this.http.get(`/assets/mocks/${id}-name.json`) as Observable<IUser>;
  }

  /**
   * Get user address
   * @param {string} id user id
   * @returns user object
   */
   getUserAddress(id?: string): Observable<IUserAddress>{
    return this.http.get(`/assets/mocks/${id}-address.json`) as Observable<IUserAddress>;
  }

  /**
   * Concat both calls and return a single object
   * 
   * @param {string} id user id
   * @returns Full user object
   */
  getUser(id: string): Observable<FullUser>{
    return forkJoin({
      userName: this.getUserName(id),
      address: this.getUserAddress(id)
    }).pipe(
      map(({userName, address}) => new FullUser(userName, address)) 
    );
  }

  /**
   * Concat both calls and return a single object (by address)
   * 
   * @param {string} id user id
   * @returns Full user object
   */
  getUserWithAddress(id: string): Observable<FullUser>{
    return this.getUserName(id).pipe(
        mergeMap((userName: IUser) => {
          if(!userName?.addressId) throw new Error('NO_ADDRESS_ID');
          return this.getUserAddress(userName.addressId).pipe(
            map((address) => new FullUser(userName, address)) 
          )
        }
      )
    )
  }
}
