export interface IUser{
    id: string;
    name: string;
    addressId?: string;
}

export interface IUserAddress{
    address: string;
    number: number;
}

export class FullUser implements IUser, IUserAddress{
    id: string;
    name: string;
    address: string;
    number: number;

    constructor({
        id,
        name
    }: IUser,{
        address,
        number
    }: IUserAddress){
        this.id = id;
        this.name = name;
        this.address = address;
        this.number = number;
    }
}