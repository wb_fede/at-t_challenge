import { Component } from '@angular/core';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'att_challenge';

  constructor(
    private _userService: UserService
  ){
    this._userService.getUser('123').subscribe(data => console.log(data));
    this._userService.getUserWithAddress('456').subscribe(data => console.log(data));
  }
}
